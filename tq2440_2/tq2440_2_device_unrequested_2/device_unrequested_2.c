#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include "sqlite3.h" 
#include "mysession.h"

char* getcgidata(FILE* fp, char* requestmethod);    //获得cgi数据
char *translate_to_chinesse (char *dest_str);       //解码

int main()
{
	char *input; 
	char *req_method; 
	char fname[256];        //名
	char fvalue[256];       //值
	int i = 0; 
	int j = 0; 
	
	FILE *fp_html;
	char buf[512]="";
	FILE *fp_text;
	
	char session_id[17];	
	
	//
	char ways1[32];
	char ways2[32];
	char session_id_t[32];
	char page[32];
	char request[32];
	
	char tNumber[32];
	char character[32];
	
	char part1[48];
	char part2[48];
	char part3[48];
	char part4[48];
	
	char temp[32];
	
	int i_section;
	
	int i_page = 1;
	
	req_method = getenv("REQUEST_METHOD");          //方法
	input = getcgidata(stdin, req_method);          //输入
	
	//-----------------------------------------------------------------------
	for ( i = 0; i < (int)strlen(input); i++ ) 
	{ 
		if ( input[i] == '=' ) 
		{ 
					 fname[j] = '\0'; 
					 break; //
		}                                    
		fname[j++] = input[i]; 
	} 
	// + "="
	for ( i = 1 + strlen(fname), j = 0; i < (int)strlen(input); i++ ) 
	{ 
		fvalue[j++] = input[i]; 
	} 
	fvalue[j] = '\0';
	j = 0;
	
	if(strcmp(fname,"session_id")==0)
	{
		stpcpy(session_id,fvalue);
		
		start_session(session_id);
		printf("%s\r\n\r\n","Content-Type:text/html");  //默认第一行输出
		
		//操作

		if( (fp_html=fopen("../tp/tp_device_unrequested_html","r")) == NULL)
		{
			exit(1);
		}
		while(fgets(buf,512,fp_html))
		{
			if(strncmp(buf,"$FLAG1$",7)==0)
			{
				printf("<li><a href='welcome_session.cgi?session_id=%s'>主页</a></li>",session_id);
				printf("<li><a href='current_msg_2.cgi?session_id=%s'>实时信息</a></li>",session_id);
				printf("<li><a href='xg_record_2.cgi?session_id=%s'>巡更数据</a></li>",session_id);
				printf("<li><a href='device_registered_2.cgi?session_id=%s'>设备列表</a></li>",session_id);
				printf("<li><a href='device_requested_show_2.cgi?session_id=%s'>授权列表</a></li>",session_id);
				printf("<li><a href='device_requested_2.cgi?session_id=%s'>设备授权</a></li>",session_id);
				printf("<li><a href='device_unrequested_show_2.cgi?session_id=%s'>注销列表</a></li>",session_id);
				printf("<li><a href='device_unrequested_2.cgi?session_id=%s'>设备注销</a></li>",session_id);
				printf("<li><a href='data_manage_2.cgi?session_id=%s'>数据库管理</a></li>",session_id);
				printf("<li><a href='destroy_session.cgi?session_id=%s'>登出</a></li>",session_id);
			}
			else if(strncmp(buf,"$FLAG2$",7)==0)
			{
				printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
				printf("<input type='submit' name='submit' value='确定' />\n");
				printf("</form>");					
			}
			else
				printf("%s",buf);
			
		}	
	}
	else if(strcmp(fname,"ways1")==0)
	{
		for ( i = 0; i < (int)strlen(fvalue); i++ ) 
		{ 
			if ( fvalue[i] == '&' ) 
			{ 
						 part1[j] = '\0'; 
						 break; //
			}                                    
			part1[j++] = fvalue[i]; 
		} 
		// + "&"
		for ( i = 1 + strlen(part1), j = 0; i < (int)strlen(fvalue); i++ ) 
		{ 
			if ( fvalue[i] == '&' ) 
			{ 
						 part2[j] = '\0'; 
						 break; //
			} 
			part2[j++] = fvalue[i]; 
		} 
		////------------------------
		for ( i = 0; i < (int)strlen(part1); i++ ) 
		{ 
			if ( part1[i] == '=' ) 
			{ 
						 temp[j] = '\0'; 
						 break; //
			}                                    
			temp[j++] = part1[i]; 
		} 
		// + "="
		for ( i = 0 + strlen(temp), j = 0; i < (int)strlen(part1); i++ ) //特别
		{ 
			ways1[j++] = part1[i]; 
		} 
		ways1[j] = '\0';
		j = 0;
		memset(temp,0,32);
		////------------------------
		for ( i = 0; i < (int)strlen(part2); i++ ) 
		{ 
			if ( part2[i] == '=' ) 
			{ 
						 temp[j] = '\0'; 
						 break; //
			}                                    
			temp[j++] = part2[i]; 
		} 
		// + "="
		for ( i = 1 + strlen(temp), j = 0; i < (int)strlen(part2); i++ ) 
		{ 
			session_id_t[j++] = part2[i]; 
		} 
		session_id_t[j] = '\0';
		j = 0;
		memset(temp,0,32);
		
		stpcpy(session_id,session_id_t);
		
		start_session(session_id);
		printf("%s\r\n\r\n","Content-Type:text/html");  //默认第一行输出
		
		//操作

		if( (fp_html=fopen("../tp/tp_device_unrequested_html","r")) == NULL)
		{
			exit(1);
		}
		while(fgets(buf,512,fp_html))
		{
			if(strncmp(buf,"$FLAG1$",7)==0)
			{
				printf("<li><a href='welcome_session.cgi?session_id=%s'>主页</a></li>",session_id);
				printf("<li><a href='current_msg_2.cgi?session_id=%s'>实时信息</a></li>",session_id);
				printf("<li><a href='xg_record_2.cgi?session_id=%s'>巡更数据</a></li>",session_id);
				printf("<li><a href='device_registered_2.cgi?session_id=%s'>设备列表</a></li>",session_id);
				printf("<li><a href='device_requested_show_2.cgi?session_id=%s'>授权列表</a></li>",session_id);
				printf("<li><a href='device_requested_2.cgi?session_id=%s'>设备授权</a></li>",session_id);
				printf("<li><a href='device_unrequested_show_2.cgi?session_id=%s'>注销列表</a></li>",session_id);
				printf("<li><a href='device_unrequested_2.cgi?session_id=%s'>设备注销</a></li>",session_id);
				printf("<li><a href='data_manage_2.cgi?session_id=%s'>数据库管理</a></li>",session_id);
				printf("<li><a href='destroy_session.cgi?session_id=%s'>登出</a></li>",session_id);
			}
			else if(strncmp(buf,"$FLAG2$",7)==0)
			{
				printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
				printf("<input type='submit' name='submit' value='确定' />\n");
				printf("</form>");
				
				
						
				//数据库查询
				
				//------------------------------------------------------------------------------------
		
				sqlite3 *db = NULL; 
				char *zErrMsg = 0; 
				int rc; 
				char sql[256] = "";
				
				rc = sqlite3_open("/myapps/dbs/zigbee.db", &db); //打开数据库
				
				//选择sql
				if(strcmp(ways1,"s0")==0)	
				{
					//创建表格，若存在，则打开
					strcpy(sql,"create table device_unrequested(id integer primary key,deviceNumber text,isCancelled text,createdTime timeStamp not null default(datetime('now','localtime')));");
					sqlite3_exec( db , sql , 0 , 0 , &zErrMsg );
					memset(sql,0,256);
					
					strcpy(sql,"SELECT * FROM device_unrequested WHERE isCancelled = 'notCancelled' ;");
					
					i_section = 0;
				}
				else 
					;
				
				//行，列，结果
				int row=0,column=0;  
				char **result;    

				//nextpage编程标志
				int pages = 0;           //
				int records = 0;         //最后一页有多少条记录
				int all_page = 0;        //共all_page页
				
				sqlite3_get_table(db,sql,&result,&row,&column,&zErrMsg);//获得表格数据

				//nextpage编程标志
				pages = row/15;
				records = row%15;
				
				//共all_page页
				if((pages==0)&&(records==0))
					all_page = 0;
				if((pages==0)&&(records>0))
					all_page = 1;
				if((pages>0)&&(records==0))
					all_page = pages;
				if((pages>0)&&(records>0))
					all_page = pages+1;
				
				
				//导出txt
				if(i_section==0)	
				{
					fp_text=fopen("../txt/duq_save.txt","w");
					fprintf(fp_text,"ID    设备号    状态    时间\n");
					for (i = 1; i < (row+1); i++)
					{
						fprintf(fp_text,"%s    %s    %s    %s\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
					}
					fclose(fp_text);
				}
				else 
					;
					
				if (row!=0)
				{
					//nextpage编程状态
					if ((pages==0)||((pages==1)&&(records==0)))  //查询到的记录只有一页
					{
						//分类
						switch(i_section)
						{
							case 0:
								printf("<p>注销请求：</p>\n");
								printf("<form name='form1' method='post'>\n");
								printf("<table id='Table1' border='1' width='1200'>\n<tr>\n<td>ID</td>\n<td>设备号</td>\n<td>状态</td>\n<td>时间</td>\n<td>提交号</td>\n<td>角色</td>\n</tr>\n");
								for (i = 1; i < (row+1); i++)
								{
									printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
								}	
								printf("<td><input type='text' name='tNumber' value='%s'/></td>\n",result[row*4+1]);
								printf("<td><input type='text' name='character'/></td>\n");			
								printf("</tr>\n");
								printf("</table>\n");	
								printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
								printf("<input type='submit' name='request' value='接受' />\n");	
								printf("<input type='submit' name='request' value='拒绝' />\n");
								printf("</form>\n");
								break;
						}
					}

					if ((((pages==1)&&(records>0))||(pages>1))&&(i_page<(pages+1)))  //查询到的记录不止一页，且当前不是最后一页
					{
					
						//分类
						switch(i_section)
						{
							case 0:
								printf("<p>注销请求：</p>\n");
								printf("<form name='form1' method='post'>\n");
								printf("<table id='Table1' border='1' width='1200'>\n<tr>\n<td>ID</td>\n<td>设备号</td>\n<td>状态</td>\n<td>时间</td>\n<td>提交号</td>\n<td>角色</td>\n</tr>\n");
								for (i = 1+(i_page-1)*15; i < i_page*15+1; i++)
								{
									printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
								}	
								printf("<td><input type='text' name='tNumber' value='%s'/></td>\n",result[row*4+1]);
								printf("<td><input type='text' name='character'/></td>\n");			
								printf("</tr>\n");
								printf("</table>\n");
								printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
								printf("<input type='submit' name='request' value='接受' />\n");	
								printf("<input type='submit' name='request' value='拒绝' />\n");
								printf("</form>\n");
								break;	
						}
							
					}

					if ((((pages==1)&&(records>0))||(pages>1))&&(i_page==(pages+1)))  //查询到的记录不止一页，且当前是最后一页
					{
						//分类
						switch(i_section)
						{
							case 0:
								printf("<p>注销请求：</p>\n");
								printf("<form name='form1' method='post'>\n");
								printf("<table id='Table1' border='1' width='1200'>\n<tr>\n<td>ID</td>\n<td>设备号</td>\n<td>状态</td>\n<td>时间</td>\n<td>提交号</td>\n<td>角色</td>\n</tr>\n");
								for (i = 1+(i_page-1)*15; i < 1+(i_page-1)*15+records; i++)
								{
									printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
								}	
								printf("<td><input type='text' name='tNumber' value='%s'/></td>\n",result[row*4+1]);
								printf("<td><input type='text' name='character'/></td>\n");			
								printf("</tr>\n");
								printf("</table>\n");	
								printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
								printf("<input type='submit' name='request' value='接受' />\n");	
								printf("<input type='submit' name='request' value='拒绝' />\n");
								printf("</form>\n");
								break;
						}			
					}

					//导出excel
					printf("<input id='Submit1' type='submit' value='导入到EXCEL' onclick='PrintTableToExcel(Table1)' />");	
					
					//------------------------------------------------------------------------------------
					
					if (i_page>1)
					{
						printf("<form name='form2' action='/cgi-bin/device_unrequested_2.cgi' method='post'>\n");
						printf("<input type='hidden' name='ways2' value='%s'/>",ways1);
						printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
						printf("<input type='hidden' name='page' value='%d'/>",(i_page-1));
						printf("<input type='submit' name='request' value='上一页' />\n");
						printf("</form>");	
					}				
					if ((i_page>0)&&(i_page<all_page))
					{
						printf("<form name='form2' action='/cgi-bin/device_unrequested_2.cgi' method='post'>\n");
						printf("<input type='hidden' name='ways2' value='%s'/>",ways1);
						printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
						printf("<input type='hidden' name='page' value='%d'/>",(i_page+1));
						printf("<input type='submit' name='request' value='下一页' />\n");
						printf("</form>");
					}
					
					printf("&nbsp;&nbsp;");
					printf("共%d页，当前为第%d页。",all_page,i_page);
					
					//下载txt
					if(i_section==0)	
					{
						printf("<a href='../txt/duq_save.txt' download='dq_save'>\n");
						printf("点击下载表单</a>\n");
					}
					else 
						;
				}		
				else
				{
					
					printf("<p>目前没有设备注销请求！</p>\n");
					
				}			
			}
			else
				printf("%s",buf);
			
		}	
	}
	else if(strcmp(fname,"ways2")==0)
	{
		for ( i = 0; i < (int)strlen(fvalue); i++ ) 
		{ 
			if ( fvalue[i] == '&' ) 
			{ 
						 part1[j] = '\0'; 
						 break; //
			}                                    
			part1[j++] = fvalue[i]; 
		} 
		// + "&"
		for ( i = 1 + strlen(part1), j = 0; i < (int)strlen(fvalue); i++ ) 
		{ 
			if ( fvalue[i] == '&' ) 
			{ 
						 part2[j] = '\0'; 
						 break; //
			} 
			part2[j++] = fvalue[i]; 
		} 
		// + "&"
		for ( i = 2 + strlen(part1) + strlen(part2), j = 0; i < (int)strlen(fvalue); i++ ) 
		{ 
			if ( fvalue[i] == '&' ) 
			{ 
						 part3[j] = '\0'; 
						 break; //
			} 
			part3[j++] = fvalue[i]; 
		}
		// + "&"
		for ( i = 3 + strlen(part1) + strlen(part2) + strlen(part3), j = 0; i < (int)strlen(fvalue); i++ ) 
		{ 
			if ( fvalue[i] == '&' ) 
			{ 
						 part4[j] = '\0'; 
						 break; //
			} 
			part4[j++] = fvalue[i]; 
		}
		
		////------------------------
		for ( i = 0; i < (int)strlen(part1); i++ ) 
		{ 
			if ( part1[i] == '=' ) 
			{ 
						 temp[j] = '\0'; 
						 break; //
			}                                    
			temp[j++] = part1[i]; 
		} 
		// + "="
		for ( i = 0 + strlen(temp), j = 0; i < (int)strlen(part1); i++ ) //特别
		{ 
			ways2[j++] = part1[i]; 
		} 
		ways2[j] = '\0';
		j = 0;
		memset(temp,0,32);
		////------------------------
		for ( i = 0; i < (int)strlen(part2); i++ ) 
		{ 
			if ( part2[i] == '=' ) 
			{ 
						 temp[j] = '\0'; 
						 break; //
			}                                    
			temp[j++] = part2[i]; 
		} 
		// + "="
		for ( i = 1 + strlen(temp), j = 0; i < (int)strlen(part2); i++ ) 
		{ 
			session_id_t[j++] = part2[i]; 
		} 
		session_id_t[j] = '\0';
		j = 0;
		memset(temp,0,32);
		////------------------------
		for ( i = 0; i < (int)strlen(part3); i++ ) 
		{ 
			if ( part3[i] == '=' ) 
			{ 
						 temp[j] = '\0'; 
						 break; //
			}                                    
			temp[j++] = part3[i]; 
		} 
		// + "="
		for ( i = 1 + strlen(temp), j = 0; i < (int)strlen(part3); i++ ) 
		{ 
			page[j++] = part3[i]; 
		} 
		page[j] = '\0';
		j = 0;
		memset(temp,0,32);
		////------------------------
		for ( i = 0; i < (int)strlen(part4); i++ ) 
		{ 
			if ( part4[i] == '=' ) 
			{ 
						 temp[j] = '\0'; 
						 break; //
			}                                    
			temp[j++] = part4[i]; 
		} 
		// + "="
		for ( i = 1 + strlen(temp), j = 0; i < (int)strlen(part4); i++ ) 
		{ 
			request[j++] = part4[i]; 
		} 
		request[j] = '\0';
		j = 0;
		memset(temp,0,32);
		
		i_page = atoi(page);
		
		//
		stpcpy(session_id,session_id_t);
		
		start_session(session_id);
		printf("%s\r\n\r\n","Content-Type:text/html");  //默认第一行输出
		
		//操作
		
		
		if( (fp_html=fopen("../tp/tp_device_unrequested_html","r")) == NULL)
		{
			exit(1);
		}
		while(fgets(buf,512,fp_html))
		{
			if(strncmp(buf,"$FLAG1$",7)==0)
			{
				printf("<li><a href='welcome_session.cgi?session_id=%s'>主页</a></li>",session_id);
				printf("<li><a href='current_msg_2.cgi?session_id=%s'>实时信息</a></li>",session_id);
				printf("<li><a href='xg_record_2.cgi?session_id=%s'>巡更数据</a></li>",session_id);
				printf("<li><a href='device_registered_2.cgi?session_id=%s'>设备列表</a></li>",session_id);
				printf("<li><a href='device_requested_show_2.cgi?session_id=%s'>授权列表</a></li>",session_id);
				printf("<li><a href='device_requested_2.cgi?session_id=%s'>设备授权</a></li>",session_id);
				printf("<li><a href='device_unrequested_show_2.cgi?session_id=%s'>注销列表</a></li>",session_id);
				printf("<li><a href='device_unrequested_2.cgi?session_id=%s'>设备注销</a></li>",session_id);
				printf("<li><a href='data_manage_2.cgi?session_id=%s'>数据库管理</a></li>",session_id);
				printf("<li><a href='destroy_session.cgi?session_id=%s'>登出</a></li>",session_id);
			}
			else if(strncmp(buf,"$FLAG2$",7)==0)
			{
				printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
				printf("<input type='submit' name='submit' value='确定' />\n");
				printf("</form>");
				

				//数据库查询
				
				//------------------------------------------------------------------------------------
		
				sqlite3 *db = NULL; 
				char *zErrMsg = 0; 
				int rc; 
				char sql[256] = "";
				
				rc = sqlite3_open("/myapps/dbs/zigbee.db", &db); //打开数据库
				
				//选择sql
				if(strcmp(ways2,"s0")==0)	
				{
					//创建表格，若存在，则打开
					strcpy(sql,"create table device_unrequested(id integer primary key,deviceNumber text,isCancelled text,createdTime timeStamp not null default(datetime('now','localtime')));");
					sqlite3_exec( db , sql , 0 , 0 , &zErrMsg );
					memset(sql,0,256);

					strcpy(sql,"SELECT * FROM device_unrequested WHERE isCancelled = 'notCancelled' ;");
					
					i_section = 0;
				}
				else 
					;
				
				//行，列，结果
				int row=0,column=0;  
				char **result;    

				//nextpage编程标志
				int pages = 0;           //
				int records = 0;         //最后一页有多少条记录
				int all_page = 0;        //共all_page页
				
				sqlite3_get_table(db,sql,&result,&row,&column,&zErrMsg);//获得表格数据

				//nextpage编程标志
				pages = row/15;
				records = row%15;
				
				//共all_page页
				if((pages==0)&&(records==0))
					all_page = 0;
				if((pages==0)&&(records>0))
					all_page = 1;
				if((pages>0)&&(records==0))
					all_page = pages;
				if((pages>0)&&(records>0))
					all_page = pages+1;
				
				
				//导出txt
				if(i_section==0)	
				{
					fp_text=fopen("../txt/duq_save.txt","w");
					fprintf(fp_text,"ID    设备号    状态    时间\n");
					for (i = 1; i < (row+1); i++)
					{
						fprintf(fp_text,"%s    %s    %s    %s\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
					}
					fclose(fp_text);
				}
				else 
					;
				
				if (row!=0)
				{
					//nextpage编程状态
					if ((pages==0)||((pages==1)&&(records==0)))  //查询到的记录只有一页
					{
						//分类
						switch(i_section)
						{
							case 0:
								printf("<p>注销请求：</p>\n");
								printf("<form name='form1' method='post'>\n");
								printf("<table id='Table1' border='1' width='1200'>\n<tr>\n<td>ID</td>\n<td>设备号</td>\n<td>状态</td>\n<td>时间</td>\n<td>提交号</td>\n<td>角色</td>\n</tr>\n");
								for (i = 1; i < (row+1); i++)
								{
									printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
								}	
								printf("<td><input type='text' name='tNumber' value='%s'/></td>\n",result[row*4+1]);
								printf("<td><input type='text' name='character'/></td>\n");				
								printf("</tr>\n");
								printf("</table>\n");	
								printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
								printf("<input type='submit' name='request' value='接受' />\n");	
								printf("<input type='submit' name='request' value='拒绝' />\n");
								printf("</form>\n");
								break;
						}
					}

					if ((((pages==1)&&(records>0))||(pages>1))&&(i_page<(pages+1)))  //查询到的记录不止一页，且当前不是最后一页
					{
					
						//分类
						switch(i_section)
						{
							case 0:
								printf("<p>注销请求：</p>\n");
								printf("<form name='form1' method='post'>\n");
								printf("<table id='Table1' border='1' width='1200'>\n<tr>\n<td>ID</td>\n<td>设备号</td>\n<td>状态</td>\n<td>时间</td>\n<td>提交号</td>\n<td>角色</td>\n</tr>\n");
								for (i = 1+(i_page-1)*15; i < i_page*15+1; i++)
								{
									printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
								}	
								printf("<td><input type='text' name='tNumber' value='%s'/></td>\n",result[row*4+1]);
								printf("<td><input type='text' name='character'/></td>\n");				
								printf("</tr>\n");
								printf("</table>\n");
								printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
								printf("<input type='submit' name='request' value='接受' />\n");	
								printf("<input type='submit' name='request' value='拒绝' />\n");
								printf("</form>\n");
								break;	
						}
							
					}

					if ((((pages==1)&&(records>0))||(pages>1))&&(i_page==(pages+1)))  //查询到的记录不止一页，且当前是最后一页
					{
						//分类
						switch(i_section)
						{
							case 0:
								printf("<p>注销请求：</p>\n");
								printf("<form name='form1' method='post'>\n");
								printf("<table id='Table1' border='1' width='1200'>\n<tr>\n<td>ID</td>\n<td>设备号</td>\n<td>状态</td>\n<td>时间</td>\n<td>提交号</td>\n<td>角色</td>\n</tr>\n");
								for (i = 1+(i_page-1)*15; i < 1+(i_page-1)*15+records; i++)
								{
									printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
								}	
								printf("<td><input type='text' name='tNumber' value='%s'/></td>\n",result[row*4+1]);
								printf("<td><input type='text' name='character'/></td>\n");			
								printf("</tr>\n");
								printf("</table>\n");	
								printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
								printf("<input type='submit' name='request' value='接受' />\n");	
								printf("<input type='submit' name='request' value='拒绝' />\n");
								printf("</form>\n");
								break;
						}			
					}

					//导出excel
					printf("<input id='Submit1' type='submit' value='导入到EXCEL' onclick='PrintTableToExcel(Table1)' />");	
					
					//------------------------------------------------------------------------------------
					
					if (i_page>1)
					{
						printf("<form name='form2' action='/cgi-bin/device_unrequested_2.cgi' method='post'>\n");
						printf("<input type='hidden' name='ways2' value='%s'/>",ways2);
						printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
						printf("<input type='hidden' name='page' value='%d'/>",(i_page-1));
						printf("<input type='submit' name='request' value='上一页' />\n");
						printf("</form>");	
					}				
					if ((i_page>0)&&(i_page<all_page))
					{
						printf("<form name='form2' action='/cgi-bin/device_unrequested_2.cgi' method='post'>\n");
						printf("<input type='hidden' name='ways2' value='%s'/>",ways2);
						printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
						printf("<input type='hidden' name='page' value='%d'/>",(i_page+1));
						printf("<input type='submit' name='request' value='下一页' />\n");
						printf("</form>");
					}
					
					printf("&nbsp;&nbsp;");
					printf("共%d页，当前为第%d页。",all_page,i_page);
					
					//下载txt
					if(i_section==0)	
					{
						printf("<a href='../txt/duq_save.txt' download='dq_save'>\n");
						printf("点击下载表单</a>\n");
					}
					else 
						;
				}		
				else
				{
					
					printf("<p>目前没有设备注销请求！</p>\n");
					
				}
			}
			else
				printf("%s",buf);
			
		}	
	}
	else if(strcmp(fname,"tNumber")==0)
	{
		for ( i = 0; i < (int)strlen(fvalue); i++ ) 
		{ 
			if ( fvalue[i] == '&' ) 
			{ 
						 part1[j] = '\0'; 
						 break; //
			}                                    
			part1[j++] = fvalue[i]; 
		} 
		// + "&"
		for ( i = 1 + strlen(part1), j = 0; i < (int)strlen(fvalue); i++ ) 
		{ 
			if ( fvalue[i] == '&' ) 
			{ 
						 part2[j] = '\0'; 
						 break; //
			} 
			part2[j++] = fvalue[i]; 
		} 
		// + "&"
		for ( i = 2 + strlen(part1) + strlen(part2), j = 0; i < (int)strlen(fvalue); i++ ) 
		{ 
			if ( fvalue[i] == '&' ) 
			{ 
						 part3[j] = '\0'; 
						 break; //
			} 
			part3[j++] = fvalue[i]; 
		}
		// + "&"
		for ( i = 3 + strlen(part1) + strlen(part2) + strlen(part3), j = 0; i < (int)strlen(fvalue); i++ ) 
		{ 
			if ( fvalue[i] == '&' ) 
			{ 
						 part4[j] = '\0'; 
						 break; //
			} 
			part4[j++] = fvalue[i]; 
		}
		
		////------------------------
		for ( i = 0; i < (int)strlen(part1); i++ ) 
		{ 
			if ( part1[i] == '=' ) 
			{ 
						 temp[j] = '\0'; 
						 break; //
			}                                    
			temp[j++] = part1[i]; 
		} 
		// + "="
		for ( i = 0 + strlen(temp), j = 0; i < (int)strlen(part1); i++ ) //特别
		{ 
			tNumber[j++] = part1[i]; 
		} 
		tNumber[j] = '\0';
		j = 0;
		memset(temp,0,32);
		////------------------------
		for ( i = 0; i < (int)strlen(part2); i++ ) 
		{ 
			if ( part2[i] == '=' ) 
			{ 
						 temp[j] = '\0'; 
						 break; //
			}                                    
			temp[j++] = part2[i]; 
		} 
		// + "="
		for ( i = 1 + strlen(temp), j = 0; i < (int)strlen(part2); i++ ) 
		{ 
			character[j++] = part2[i]; 
		} 
		character[j] = '\0';
		j = 0;
		memset(temp,0,32);
		////------------------------
		for ( i = 0; i < (int)strlen(part3); i++ ) 
		{ 
			if ( part3[i] == '=' ) 
			{ 
						 temp[j] = '\0'; 
						 break; //
			}                                    
			temp[j++] = part3[i]; 
		} 
		// + "="
		for ( i = 1 + strlen(temp), j = 0; i < (int)strlen(part3); i++ ) 
		{ 
			session_id_t[j++] = part3[i]; 
		} 
		session_id_t[j] = '\0';
		j = 0;
		memset(temp,0,32);
		////------------------------
		for ( i = 0; i < (int)strlen(part4); i++ ) 
		{ 
			if ( part4[i] == '=' ) 
			{ 
						 temp[j] = '\0'; 
						 break; //
			}                                    
			temp[j++] = part4[i]; 
		} 
		// + "="
		for ( i = 1 + strlen(temp), j = 0; i < (int)strlen(part4); i++ ) 
		{ 
			request[j++] = part4[i]; 
		} 
		request[j] = '\0';
		j = 0;
		memset(temp,0,32);
			
		//
		stpcpy(session_id,session_id_t);
		
		start_session(session_id);
		printf("%s\r\n\r\n","Content-Type:text/html");  //默认第一行输出
		
		//操作
		
		
		if( (fp_html=fopen("../tp/tp_device_unrequested_html","r")) == NULL)
		{
			exit(1);
		}
		while(fgets(buf,512,fp_html))
		{
			if(strncmp(buf,"$FLAG1$",7)==0)
			{
				printf("<li><a href='welcome_session.cgi?session_id=%s'>主页</a></li>",session_id);
				printf("<li><a href='current_msg_2.cgi?session_id=%s'>实时信息</a></li>",session_id);
				printf("<li><a href='xg_record_2.cgi?session_id=%s'>巡更数据</a></li>",session_id);
				printf("<li><a href='device_registered_2.cgi?session_id=%s'>设备列表</a></li>",session_id);
				printf("<li><a href='device_requested_show_2.cgi?session_id=%s'>授权列表</a></li>",session_id);
				printf("<li><a href='device_requested_2.cgi?session_id=%s'>设备授权</a></li>",session_id);
				printf("<li><a href='device_unrequested_show_2.cgi?session_id=%s'>注销列表</a></li>",session_id);
				printf("<li><a href='device_unrequested_2.cgi?session_id=%s'>设备注销</a></li>",session_id);
				printf("<li><a href='data_manage_2.cgi?session_id=%s'>数据库管理</a></li>",session_id);
				printf("<li><a href='destroy_session.cgi?session_id=%s'>登出</a></li>",session_id);
			}
			else if(strncmp(buf,"$FLAG2$",7)==0)
			{
				printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
				printf("<input type='submit' name='submit' value='确定' />\n");
				printf("</form>");
				

				//数据库查询
				
				//------------------------------------------------------------------------------------
		
				sqlite3 *db = NULL; 
				char *zErrMsg = 0; 
				int rc; 
				char sql[256] = "";
				
				char mid[] = "','";
				char end1[] = "');";
				char end2[] = "';";	
				
				rc = sqlite3_open("/myapps/dbs/zigbee.db", &db); //打开数据库
				
				if(strcmp(request,"%BE%DC%BE%F8")==0)  //拒绝
				{
					i_section = 1;
				}
				if(strcmp(request,"%BD%D3%CA%DC")==0)  //接受
				{
					i_section = 2;
				}
				
				//选择sql
				if(i_section == 1)	
				{
					//创建表格，若存在，则打开
					strcpy(sql,"create table device_unrequested(id integer primary key,deviceNumber text,isCancelled text,createdTime timeStamp not null default(datetime('now','localtime')));");
					sqlite3_exec( db , sql , 0 , 0 , &zErrMsg );
					memset(sql,0,256);
					
					strcpy(sql," update device_unrequested set isCancelled =  '不注销' WHERE deviceNumber = '");
					strcat(sql,tNumber);
					strcat(sql,end2);	
					sqlite3_exec( db , sql , 0 , 0 , &zErrMsg );
					memset(sql,0,256);

					strcpy(sql,"SELECT * FROM device_unrequested WHERE isCancelled = 'notCancelled' ;");
				}
				if(i_section == 2)	
				{
					//创建表格，若存在，则打开
					strcpy(sql,"create table device_unrequested(id integer primary key,deviceNumber text,isCancelled text,createdTime timeStamp not null default(datetime('now','localtime')));");
					sqlite3_exec( db , sql , 0 , 0 , &zErrMsg );
					memset(sql,0,256);
					
					strcpy(sql," update device_unrequested set isCancelled =  '已注销' WHERE deviceNumber = '");
					strcat(sql,tNumber);
					strcat(sql,end2);
					sqlite3_exec( db , sql , 0 , 0 , &zErrMsg );
					memset(sql,0,256);
					
					if(strcmp(character,"note")==0)  //节点
					{
						//创建表格，若存在，则打开
						strcpy(sql,"create table note_registered(id integer primary key,noteNumber text,place text,flag text,description text,createdTime timeStamp not null default(datetime('now','localtime')));");
						sqlite3_exec( db , sql , 0 , 0 , &zErrMsg );
						memset(sql,0,256);
						
						strcpy(sql,"delete from note_registered where noteNumber='");//delete from note_registered where noteNumber=''
						strcat(sql,tNumber);
						strcat(sql,end2);
					}
					if(strcmp(character,"card")==0)  //卡片
					{
						//创建表格，若存在，则打开
						strcpy(sql,"create table card_registered(id integer primary key,cardNumber text,who text,flag text,description text,createdTime timeStamp not null default(datetime('now','localtime')));");
						sqlite3_exec( db , sql , 0 , 0 , &zErrMsg );
						memset(sql,0,256);
						
						strcpy(sql,"delete from card_registered where cardNumber='");//delete from card_registered where cardNumber=''
						strcat(sql,tNumber);
						strcat(sql,end2);
					}
					sqlite3_exec( db , sql , 0 , 0 , &zErrMsg );
					memset(sql,0,256);
					
					
					//创建表格，若存在，则打开
					strcpy(sql,"create table device_unrequested(id integer primary key,deviceNumber text,isCancelled text,createdTime timeStamp not null default(datetime('now','localtime')));");
					sqlite3_exec( db , sql , 0 , 0 , &zErrMsg );
					memset(sql,0,256);

					strcpy(sql,"SELECT * FROM device_unrequested WHERE isCancelled = 'notCancelled' ;");
				}
				else 
					;
				
				//行，列，结果
				int row=0,column=0;  
				char **result;    

				//nextpage编程标志
				int pages = 0;           //
				int records = 0;         //最后一页有多少条记录
				int all_page = 0;        //共all_page页
				
				sqlite3_get_table(db,sql,&result,&row,&column,&zErrMsg);//获得表格数据

				//nextpage编程标志
				pages = row/15;
				records = row%15;
				
				//共all_page页
				if((pages==0)&&(records==0))
					all_page = 0;
				if((pages==0)&&(records>0))
					all_page = 1;
				if((pages>0)&&(records==0))
					all_page = pages;
				if((pages>0)&&(records>0))
					all_page = pages+1;
				
				
				//导出txt
				if((i_section==1)||(i_section==2))	
				{
					fp_text=fopen("../txt/duq_save.txt","w");
					fprintf(fp_text,"ID    设备号    状态    时间\n");
					for (i = 1; i < (row+1); i++)
					{
						fprintf(fp_text,"%s    %s    %s    %s\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
					}
					fclose(fp_text);
				}
				else 
					;
				
				if (row!=0)
				{
					//nextpage编程状态
					if ((pages==0)||((pages==1)&&(records==0)))  //查询到的记录只有一页
					{
						//分类
						switch(i_section)
						{
							case 1:
								printf("<p>注销请求：</p>\n");
								printf("<form name='form1' method='post'>\n");
								printf("<table id='Table1' border='1' width='1200'>\n<tr>\n<td>ID</td>\n<td>设备号</td>\n<td>状态</td>\n<td>时间</td>\n<td>提交号</td>\n<td>角色</td>\n</tr>\n");
								for (i = 1; i < (row+1); i++)
								{
									printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
								}	
								printf("<td><input type='text' name='tNumber' value='%s'/></td>\n",result[row*4+1]);
								printf("<td><input type='text' name='character'/></td>\n");				
								printf("</tr>\n");
								printf("</table>\n");	
								printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
								printf("<input type='submit' name='request' value='接受' />\n");	
								printf("<input type='submit' name='request' value='拒绝' />\n");
								printf("</form>\n");
								break;
								
							case 2:
								printf("<p>注销请求：</p>\n");
								printf("<form name='form1' method='post'>\n");
								printf("<table id='Table1' border='1' width='1200'>\n<tr>\n<td>ID</td>\n<td>设备号</td>\n<td>状态</td>\n<td>时间</td>\n<td>提交号</td>\n<td>角色</td>\n</tr>\n");
								for (i = 1; i < (row+1); i++)
								{
									printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
								}	
								printf("<td><input type='text' name='tNumber' value='%s'/></td>\n",result[row*4+1]);
								printf("<td><input type='text' name='character'/></td>\n");			
								printf("</tr>\n");
								printf("</table>\n");	
								printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
								printf("<input type='submit' name='request' value='接受' />\n");	
								printf("<input type='submit' name='request' value='拒绝' />\n");
								printf("</form>\n");
								break;
						}
					}

					if ((((pages==1)&&(records>0))||(pages>1))&&(i_page<(pages+1)))  //查询到的记录不止一页，且当前不是最后一页
					{
					
						//分类
						switch(i_section)
						{
							case 1:
								printf("<p>注销请求：</p>\n");
								printf("<form name='form1' method='post'>\n");
								printf("<table id='Table1' border='1' width='1200'>\n<tr>\n<td>ID</td>\n<td>设备号</td>\n<td>状态</td>\n<td>时间</td>\n<td>提交号</td>\n<td>角色</td>\n</tr>\n");
								for (i = 1+(i_page-1)*15; i < i_page*15+1; i++)
								{
									printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
								}	
								printf("<td><input type='text' name='tNumber' value='%s'/></td>\n",result[row*4+1]);
								printf("<td><input type='text' name='character'/></td>\n");			
								printf("</tr>\n");
								printf("</table>\n");
								printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
								printf("<input type='submit' name='request' value='接受' />\n");	
								printf("<input type='submit' name='request' value='拒绝' />\n");
								printf("</form>\n");
								break;

							case 2:
								printf("<p>注销请求：</p>\n");
								printf("<form name='form1' method='post'>\n");
								printf("<table id='Table1' border='1' width='1200'>\n<tr>\n<td>ID</td>\n<td>设备号</td>\n<td>状态</td>\n<td>时间</td>\n<td>提交号</td>\n<td>角色</td>\n</tr>\n");
								for (i = 1; i < (row+1); i++)
								{
									printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
								}	
								printf("<td><input type='text' name='tNumber' value='%s'/></td>\n",result[row*4+1]);
								printf("<td><input type='text' name='character'/></td>\n");			
								printf("</tr>\n");
								printf("</table>\n");	
								printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
								printf("<input type='submit' name='request' value='接受' />\n");	
								printf("<input type='submit' name='request' value='拒绝' />\n");
								printf("</form>\n");
								break;
						}
							
					}

					if ((((pages==1)&&(records>0))||(pages>1))&&(i_page==(pages+1)))  //查询到的记录不止一页，且当前是最后一页
					{
						//分类
						switch(i_section)
						{
							case 1:
								printf("<p>注销请求：</p>\n");
								printf("<form name='form1' method='post'>\n");
								printf("<table id='Table1' border='1' width='1200'>\n<tr>\n<td>ID</td>\n<td>设备号</td>\n<td>状态</td>\n<td>时间</td>\n<td>提交号</td>\n<td>角色</td>\n</tr>\n");
								for (i = 1+(i_page-1)*15; i < 1+(i_page-1)*15+records; i++)
								{
									printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
								}	
								printf("<td><input type='text' name='tNumber' value='%s'/></td>\n",result[row*4+1]);
								printf("<td><input type='text' name='character'/></td>\n");			
								printf("</tr>\n");
								printf("</table>\n");	
								printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
								printf("<input type='submit' name='request' value='接受' />\n");	
								printf("<input type='submit' name='request' value='拒绝' />\n");
								printf("</form>\n");
								break;
								
							case 2:
								printf("<p>注销请求：</p>\n");
								printf("<form name='form1' method='post'>\n");
								printf("<table id='Table1' border='1' width='1200'>\n<tr>\n<td>ID</td>\n<td>设备号</td>\n<td>状态</td>\n<td>时间</td>\n<td>提交号</td>\n<td>角色</td>\n</tr>\n");
								for (i = 1; i < (row+1); i++)
								{
									printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
								}	
								printf("<td><input type='text' name='tNumber' value='%s'/></td>\n",result[row*4+1]);
								printf("<td><input type='text' name='character'/></td>\n");			
								printf("</tr>\n");
								printf("</table>\n");	
								printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
								printf("<input type='submit' name='request' value='接受' />\n");	
								printf("<input type='submit' name='request' value='拒绝' />\n");
								printf("</form>\n");
								break;
						}			
					}

					//导出excel
					printf("<input id='Submit1' type='submit' value='导入到EXCEL' onclick='PrintTableToExcel(Table1)' />");	
					
					//------------------------------------------------------------------------------------
					
					if (i_page>1)
					{
						printf("<form name='form2' action='/cgi-bin/device_unrequested_2.cgi' method='post'>\n");
						printf("<input type='hidden' name='ways2' value='s0'/>");
						printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
						printf("<input type='hidden' name='page' value='%d'/>",(i_page-1));
						printf("<input type='submit' name='request' value='上一页' />\n");
						printf("</form>");	
					}				
					if ((i_page>0)&&(i_page<all_page))
					{
						printf("<form name='form2' action='/cgi-bin/device_unrequested_2.cgi' method='post'>\n");
						printf("<input type='hidden' name='ways2' value='s0'/>");
						printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
						printf("<input type='hidden' name='page' value='%d'/>",(i_page+1));
						printf("<input type='submit' name='request' value='下一页' />\n");
						printf("</form>");
					}
					
					printf("&nbsp;&nbsp;");
					printf("共%d页，当前为第%d页。",all_page,i_page);
					
					//下载txt
					if(i_section==0)	
					{
						printf("<a href='../txt/duq_save.txt' download='dq_save'>\n");
						printf("点击下载表单</a>\n");
					}
					else 
						;
				}		
				else
				{
					
					printf("<p>目前没有设备注销请求！</p>\n");
					
				}
			}
			else
				printf("%s",buf);
			
		}
		
	}
	else
		;
 
	return 0; 
         
} 




char* getcgidata(FILE* fp, char* requestmethod) 
{ 
	char* input; 
	int len; 
	int size = 1024; 
	int i = 0; 
	
	if (!strcmp(requestmethod, "GET")) 
	{ 
		input = getenv("QUERY_STRING"); 
		return input; 
	} 
	else if (!strcmp(requestmethod, "POST")) 
	{ 
		len = atoi(getenv("CONTENT_LENGTH")); 
		input = (char*)malloc(sizeof(char)*(size + 1)); 
	 
		if (len == 0) 
		{ 
			input[0] = '\0'; 
			return input; 
		} 
	 
		while(1) 
		{ 
			input[i] = (char)fgetc(fp); 
			if (i == size) 
			{ 
				input[i+1] = '\0'; 
				return input; 
			} 
			
			--len; 
			if (feof(fp) || (!(len))) 
			{ 
				i++; 
				input[i] = '\0'; 
				return input; 
			} 
			i++; 
			
		} 
	} 
	return NULL; 
}

char *translate_to_chinesse (char *dest_str) 
{ 
  register int x = 0, y = 0; 
  char *temp_str = NULL; 
  char ascii_one,ascii_two; 
  temp_str = (char *)malloc(strlen(dest_str) * sizeof(char) +1);
  while(dest_str[x]) 
  {
      if((temp_str[x] = dest_str[y]) == '%') 
      { 
          if(dest_str[y+1] >= 'A') 
              ascii_one = (( dest_str[y+1] & 0xdf)-'A') +10;
          else
              ascii_one = dest_str[y+1] - '0'; 
          if(dest_str[y+2] >= 'A')
              ascii_two = ((dest_str[y+2] & 0xdf) - 'A') + 10; 
          else
              ascii_two = dest_str[y+2] -'0'; 
          temp_str[x] = ascii_one * 16 + ascii_two;
          y += 2;
      }
      x++;
      y++;
  }
  temp_str[x] = '\0';
  return (temp_str); 
}