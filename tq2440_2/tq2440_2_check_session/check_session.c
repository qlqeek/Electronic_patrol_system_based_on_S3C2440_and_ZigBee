#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include "sqlite3.h" 
#include "mysession.h"

char* getcgidata(FILE* fp, char* requestmethod); 
int main() 
{ 
	 char *input; 
	 char *req_method; 
	 char namein[12]; 
	 char pass[12]; 
	 char passtemp[12]; 
	 int i = 0; 
	 int j = 0;
	 
	 //
	 char *session_id;

	FILE *fp_html;
	char buf[512]="";
	
	 //printf("Content-type: text/html\n\n"); 
	 /* printf("%s\r\n\r\n","Content-Type:text/html");	 */
	
	 req_method = getenv("REQUEST_METHOD"); 
	 input = getcgidata(stdin, req_method); 
	 // 我们获取的input字符串可能像如下的形式 
	 // Username="admin"&Password="aaaaa" 
	 // 其中"Username="和"&Password="都是固定的 
	 // 而"admin"和"aaaaa"都是变化的，也是我们要获取的 
	 // 前面9个字符是Usernamein= 
	 // 在"Username="和"&"之间的是我们要取出来的用户名 
	 for ( i = 9; i < (int)strlen(input); i++ ) 
	 { 
		if ( input[i] == '&' ) 
		{ 
					 namein[j] = '\0'; 
					 break; //跳出for循环，因为用户名已经取得。
		}                                    
		namein[j++] = input[i]; 
	 } 
 // 前面9个字符 + "&Password="10个字符
	 for ( i = 19 + strlen(namein), j = 0; i < (int)strlen(input); i++ ) 
	 { 
		pass[j++] = input[i]; 
	 } 
	 pass[j] = '\0'; 

	 
	sqlite3_stmt*    stmt; 
		
	sqlite3 *db=NULL; 
	char *zErrMsg = 0; 
	int rc; 
	rc = sqlite3_open("/myapps/dbs/zigbee.db", &db); //打开指定的数据库文件,如果不存在将创建一个同名的数据库文件 

	//创建一个表,如果该表存在，则不创建，并给出提示信息，存储在 zErrMsg 中 
	char *sql = " CREATE TABLE user(ID INTEGER PRIMARY KEY,Name text,Password text);" ; 
	sqlite3_exec( db , sql , 0 , 0 , &zErrMsg ); 
		
	//查询数据
	int ret=0; 
	int t=0; 
	sql = "SELECT Password FROM user WHERE Name=?"; 
	ret = sqlite3_prepare(db,sql,strlen(sql),&stmt,NULL); 
	sqlite3_bind_text(stmt, 1, namein,strlen(namein),SQLITE_STATIC); 
	while( sqlite3_step(stmt) == SQLITE_ROW)
	{ 
		strcpy(passtemp,sqlite3_column_text(stmt,0)); 
		if(strcmp(passtemp,pass)==0) 
			{ 				
				
				session_id = set_session(namein,passtemp);
				printf("%s\r\n\r\n","Content-Type:text/html");	
				printf("<script language=\"JavaScript\">self.location='/cgi-bin/welcome_session.cgi?session_id=%s';</script>",session_id);
				
				t=1; 
		} 
	} 
	if(t==0) 
	{ 
		
		session_id = set_session(namein,passtemp);
		printf("%s\r\n\r\n","Content-Type:text/html");	
		
		if( (fp_html=fopen("../tp/tp_index_html","r")) == NULL)
		{
			exit(1);
		}
		while(fgets(buf,512,fp_html))
		{
			printf("%s",buf);
			
		}
	}
		
	sqlite3_finalize(stmt); 	
	sqlite3_close(db); //关闭数据库 
	return 0; 
         
} 




char* getcgidata(FILE* fp, char* requestmethod) 
{ 
	char* input; 
	int len; 
	int size = 1024; 
	int i = 0; 
	
	if (!strcmp(requestmethod, "GET")) 
	{ 
		input = getenv("QUERY_STRING"); 
		return input; 
	} 
	else if (!strcmp(requestmethod, "POST")) 
	{ 
		len = atoi(getenv("CONTENT_LENGTH")); 
		input = (char*)malloc(sizeof(char)*(size + 1)); 
	 
		if (len == 0) 
		{ 
			input[0] = '\0'; 
			return input; 
		} 
	 
		while(1) 
		{ 
			input[i] = (char)fgetc(fp); 
			if (i == size) 
			{ 
				input[i+1] = '\0'; 
				return input; 
			} 
			
			--len; 
			if (feof(fp) || (!(len))) 
			{ 
				i++; 
				input[i] = '\0'; 
				return input; 
			} 
			i++; 
			
		} 
	} 
	return NULL; 
}
