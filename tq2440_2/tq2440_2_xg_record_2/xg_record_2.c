#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include "sqlite3.h" 
#include "mysession.h"

char* getcgidata(FILE* fp, char* requestmethod);    //获得cgi数据

int main()
{
	char *input; 
	char *req_method; 
	char fname[128];        //名
	char fvalue[128];       //值
	int i = 0; 
	int j = 0; 
	
	FILE *fp_html;
	char buf[512]="";
	FILE *fp_text;
	
	char session_id[17];	
	
	//
	char ways1[32];
	char ways2[32];
	char session_id_t[32];
	char page[32];
	char request[32];
	
	char part1[48];
	char part2[48];
	char part3[48];
	char part4[48];
	
	char temp[32];
	
	int i_section;
	
	int i_page = 1;
	
	req_method = getenv("REQUEST_METHOD");          //方法
	input = getcgidata(stdin, req_method);          //输入
	
	//-----------------------------------------------------------------------
	for ( i = 0; i < (int)strlen(input); i++ ) 
	{ 
		if ( input[i] == '=' ) 
		{ 
					 fname[j] = '\0'; 
					 break; //
		}                                    
		fname[j++] = input[i]; 
	} 
	// + "="
	for ( i = 1 + strlen(fname), j = 0; i < (int)strlen(input); i++ ) 
	{ 
		fvalue[j++] = input[i]; 
	} 
	fvalue[j] = '\0';
	j = 0;
	
	if(strcmp(fname,"session_id")==0)
	{
		stpcpy(session_id,fvalue);
		
		start_session(session_id);
		printf("%s\r\n\r\n","Content-Type:text/html");  //默认第一行输出
		
		//操作

		if( (fp_html=fopen("../tp/tp_xg_record_html","r")) == NULL)
		{
			exit(1);
		}
		while(fgets(buf,512,fp_html))
		{
			if(strncmp(buf,"$FLAG1$",7)==0)
			{
				printf("<li><a href='welcome_session.cgi?session_id=%s'>主页</a></li>",session_id);
				printf("<li><a href='current_msg_2.cgi?session_id=%s'>实时信息</a></li>",session_id);
				printf("<li><a href='xg_record_2.cgi?session_id=%s'>巡更数据</a></li>",session_id);
				printf("<li><a href='device_registered_2.cgi?session_id=%s'>设备列表</a></li>",session_id);
				printf("<li><a href='device_requested_show_2.cgi?session_id=%s'>授权列表</a></li>",session_id);
				printf("<li><a href='device_requested_2.cgi?session_id=%s'>设备授权</a></li>",session_id);
				printf("<li><a href='device_unrequested_show_2.cgi?session_id=%s'>注销列表</a></li>",session_id);
				printf("<li><a href='device_unrequested_2.cgi?session_id=%s'>设备注销</a></li>",session_id);
				printf("<li><a href='data_manage_2.cgi?session_id=%s'>数据库管理</a></li>",session_id);
				printf("<li><a href='destroy_session.cgi?session_id=%s'>登出</a></li>",session_id);
			}
			else if(strncmp(buf,"$FLAG2$",7)==0)
			{
				printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
				printf("<input type='submit' name='submit' value='确定' />\n");
				printf("</form>");				
			}
			else
				printf("%s",buf);
			
		}	
	}
	else if(strcmp(fname,"ways1")==0)
	{
		for ( i = 0; i < (int)strlen(fvalue); i++ ) 
		{ 
			if ( fvalue[i] == '&' ) 
			{ 
						 part1[j] = '\0'; 
						 break; //
			}                                    
			part1[j++] = fvalue[i]; 
		} 
		// + "&"
		for ( i = 1 + strlen(part1), j = 0; i < (int)strlen(fvalue); i++ ) 
		{ 
			if ( fvalue[i] == '&' ) 
			{ 
						 part2[j] = '\0'; 
						 break; //
			} 
			part2[j++] = fvalue[i]; 
		} 
		////------------------------
		for ( i = 0; i < (int)strlen(part1); i++ ) 
		{ 
			if ( part1[i] == '=' ) 
			{ 
						 temp[j] = '\0'; 
						 break; //
			}                                    
			temp[j++] = part1[i]; 
		} 
		// + "="
		for ( i = 0 + strlen(temp), j = 0; i < (int)strlen(part1); i++ ) //特别
		{ 
			ways1[j++] = part1[i]; 
		} 
		ways1[j] = '\0';
		j = 0;
		memset(temp,0,32);
		////------------------------
		for ( i = 0; i < (int)strlen(part2); i++ ) 
		{ 
			if ( part2[i] == '=' ) 
			{ 
						 temp[j] = '\0'; 
						 break; //
			}                                    
			temp[j++] = part2[i]; 
		} 
		// + "="
		for ( i = 1 + strlen(temp), j = 0; i < (int)strlen(part2); i++ ) 
		{ 
			session_id_t[j++] = part2[i]; 
		} 
		session_id_t[j] = '\0';
		j = 0;
		memset(temp,0,32);
		
		stpcpy(session_id,session_id_t);
		
		start_session(session_id);
		printf("%s\r\n\r\n","Content-Type:text/html");  //默认第一行输出
		
		//操作

		if( (fp_html=fopen("../tp/tp_xg_record_html","r")) == NULL)
		{
			exit(1);
		}
		while(fgets(buf,512,fp_html))
		{
			if(strncmp(buf,"$FLAG1$",7)==0)
			{
				printf("<li><a href='welcome_session.cgi?session_id=%s'>主页</a></li>",session_id);
				printf("<li><a href='current_msg_2.cgi?session_id=%s'>实时信息</a></li>",session_id);
				printf("<li><a href='xg_record_2.cgi?session_id=%s'>巡更数据</a></li>",session_id);
				printf("<li><a href='device_registered_2.cgi?session_id=%s'>设备列表</a></li>",session_id);
				printf("<li><a href='device_requested_show_2.cgi?session_id=%s'>授权列表</a></li>",session_id);
				printf("<li><a href='device_requested_2.cgi?session_id=%s'>设备授权</a></li>",session_id);
				printf("<li><a href='device_unrequested_show_2.cgi?session_id=%s'>注销列表</a></li>",session_id);
				printf("<li><a href='device_unrequested_2.cgi?session_id=%s'>设备注销</a></li>",session_id);
				printf("<li><a href='data_manage_2.cgi?session_id=%s'>数据库管理</a></li>",session_id);
				printf("<li><a href='destroy_session.cgi?session_id=%s'>登出</a></li>",session_id);
			}
			else if(strncmp(buf,"$FLAG2$",7)==0)
			{
				printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
				printf("<input type='submit' name='submit' value='确定' />\n");
				printf("</form>");
				
				//数据库查询
				
				//------------------------------------------------------------------------------------
		
				sqlite3 *db = NULL; 
				char *zErrMsg = 0; 
				int rc; 
				char *sql;
				
				rc = sqlite3_open("/myapps/dbs/zigbee.db", &db); //打开数据库

				//创建表格，若存在，则打开
				sql = " create table xg_record(id integer primary key,thisPoint text,who text,createdTime timeStamp not null default(datetime('now','localtime'))); " ; 
				sqlite3_exec( db , sql , 0 , 0 , &zErrMsg ); 
				
				//选择sql
				if(strcmp(ways1,"s1")==0)	
				{
					sql = "SELECT * FROM xg_record WHERE DATE('now', '-3 day', 'localtime') < DATE(createdTime) < DATE('now', '-0 day', 'localtime');";
					i_section = 1;
				}
				else if(strcmp(ways1,"s2")==0)
				{
					sql = "SELECT * FROM xg_record WHERE DATE('now', '-5 day', 'localtime') < DATE(createdTime) < DATE('now', '-0 day', 'localtime');";
					i_section = 2;
				}
				else if(strcmp(ways1,"s3")==0)
				{
					sql = "SELECT * FROM xg_record WHERE DATE('now', '-7 day', 'localtime') < DATE(createdTime) < DATE('now', '-0 day', 'localtime');";
					i_section = 3;
				}
				else if(strcmp(ways1,"s4")==0)
				{
					sql = " SELECT * FROM xg_record WHERE who = '王小明'; ";
					i_section = 4;
				}
				else if(strcmp(ways1,"s5")==0)
				{
					sql = " SELECT * FROM xg_record WHERE who = '王小东'; ";
					i_section = 5;
				}
				else if(strcmp(ways1,"s6")==0)
				{
					sql = " SELECT * FROM xg_record WHERE who = '王小西'; ";
					i_section = 6;
				}
				else if(strcmp(ways1,"s7")==0)
				{
					sql = " SELECT * FROM xg_record; ";
					i_section = 7;
				}
				else sql = "";
				
				//test
				/* printf("sql:%s",sql); */
				
				//行，列，结果
				int row=0,column=0;  
				char **result;    

				//nextpage编程标志
				int pages = 0;           //
				int records = 0;         //最后一页有多少条记录
				int all_page = 0;        //共all_page页
				
				sqlite3_get_table(db,sql,&result,&row,&column,&zErrMsg);//获得表格数据

				//nextpage编程标志
				pages = row/15;
				records = row%15;
				
				//共all_page页
				if((pages==0)&&(records==0))
					all_page = 0;
				if((pages==0)&&(records>0))
					all_page = 1;
				if((pages>0)&&(records==0))
					all_page = pages;
				if((pages>0)&&(records>0))
					all_page = pages+1;
				
				//test
				/* printf("pages=%d",pages);
				printf("records=%d",records);
				printf("all_page=%d",all_page); */
				
				//导出txt
				fp_text=fopen("../txt/xg_save.txt","w");
				fprintf(fp_text,"ID    本节点    巡更人    时间\n");
				for (i = 1; i < (row+1); i++)
				{
					fprintf(fp_text,"%s    %s    %s    %s    %s\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
				}	
				fclose(fp_text);
		
				//nextpage编程标志
				if ((pages==0)||((pages==1)&&(records==0)))  //查询到的记录只有一页
				{
					//分类
					switch(i_section)
					{
						case 1:
							printf("<p>三日内的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1; i < (row+1); i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}		
							printf("</table>\n");
							break;
						case 2:
							printf("<p>五日内的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1; i < (row+1); i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}		
							printf("</table>\n");
							break;
						case 3:
							printf("<p>七日内的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1; i < (row+1); i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;
						case 4:
							printf("<p>王小明的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1; i < (row+1); i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;	
						case 5:
							printf("<p>王小东的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1; i < (row+1); i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;	
						case 6:
							printf("<p>王小西的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1; i < (row+1); i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;	
						case 7:
							printf("<p>全部节点的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1; i < (row+1); i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;		
						
					}
				}

				if ((((pages==1)&&(records>0))||(pages>1))&&(i_page<(pages+1)))  //查询到的记录不止一页，且当前不是最后一页
				{
				
					//分类
					switch(i_section)
					{
						case 1:
							printf("<p>三日内的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < i_page*15+1; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}		
							printf("</table>\n");
							break;
						case 2:
							printf("<p>五日内的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < i_page*15+1; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}		
							printf("</table>\n");
							break;
						case 3:
							printf("<p>七日内的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < i_page*15+1; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;
						case 4:
							printf("<p>王小明的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < i_page*15+1; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;	
						case 5:
							printf("<p>王小东的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < i_page*15+1; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;	
						case 6:
							printf("<p>王小西的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < i_page*15+1; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;	
						case 7:
							printf("<p>全部节点的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < i_page*15+1; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;		
						
					}
						
				}

				if ((((pages==1)&&(records>0))||(pages>1))&&(i_page==(pages+1)))  //查询到的记录不止一页，且当前是最后一页
				{
					//分类
					switch(i_section)
					{
						case 1:
							printf("<p>三日内的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < 1+(i_page-1)*15+records; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}		
							printf("</table>\n");
							break;
						case 2:
							printf("<p>五日内的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < 1+(i_page-1)*15+records; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}		
							printf("</table>\n");
							break;
						case 3:
							printf("<p>七日内的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < 1+(i_page-1)*15+records; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;
						case 4:
							printf("<p>王小明的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < 1+(i_page-1)*15+records; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;	
						case 5:
							printf("<p>王小东的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < 1+(i_page-1)*15+records; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;	
						case 6:
							printf("<p>王小西的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < 1+(i_page-1)*15+records; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;	
						case 7:
							printf("<p>全部节点的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < 1+(i_page-1)*15+records; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;		
						
					}			
				}

				//导出excel
				printf("<input id='Submit1' type='submit' value='导入到EXCEL' onclick='PrintTableToExcel(Table1)' />");	
				
				//------------------------------------------------------------------------------------
				
				if (i_page>1)
				{
					printf("<form name='form2' action='/cgi-bin/xg_record_2.cgi' method='post'>\n");
					printf("<input type='hidden' name='ways2' value='%s'/>",ways1);
					printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
					printf("<input type='hidden' name='page' value='%d'/>",(i_page-1));
					printf("<input type='submit' name='request' value='上一页' />\n");
					printf("</form>");	
				}				
				if ((i_page>0)&&(i_page<all_page))
				{
					printf("<form name='form2' action='/cgi-bin/xg_record_2.cgi' method='post'>\n");
					printf("<input type='hidden' name='ways2' value='%s'/>",ways1);
					printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
					printf("<input type='hidden' name='page' value='%d'/>",(i_page+1));
					printf("<input type='submit' name='request' value='下一页' />\n");
					printf("</form>");
				}
				
				printf("&nbsp;&nbsp;");
				printf("共%d页，当前为第%d页。",all_page,i_page);
				
				//下载txt
				printf("<a href='../txt/xg_save.txt' download='xg_save'>\n");
				printf("点击下载表单</a>\n");
					
			}
			else
				printf("%s",buf);
			
		}	
	}
	else if(strcmp(fname,"ways2")==0)
	{
		for ( i = 0; i < (int)strlen(fvalue); i++ ) 
		{ 
			if ( fvalue[i] == '&' ) 
			{ 
						 part1[j] = '\0'; 
						 break; //
			}                                    
			part1[j++] = fvalue[i]; 
		} 
		// + "&"
		for ( i = 1 + strlen(part1), j = 0; i < (int)strlen(fvalue); i++ ) 
		{ 
			if ( fvalue[i] == '&' ) 
			{ 
						 part2[j] = '\0'; 
						 break; //
			} 
			part2[j++] = fvalue[i]; 
		} 
		// + "&"
		for ( i = 2 + strlen(part1) + strlen(part2), j = 0; i < (int)strlen(fvalue); i++ ) 
		{ 
			if ( fvalue[i] == '&' ) 
			{ 
						 part3[j] = '\0'; 
						 break; //
			} 
			part3[j++] = fvalue[i]; 
		}
		// + "&"
		for ( i = 3 + strlen(part1) + strlen(part2) + strlen(part3), j = 0; i < (int)strlen(fvalue); i++ ) 
		{ 
			if ( fvalue[i] == '&' ) 
			{ 
						 part4[j] = '\0'; 
						 break; //
			} 
			part4[j++] = fvalue[i]; 
		}
		
		////------------------------
		for ( i = 0; i < (int)strlen(part1); i++ ) 
		{ 
			if ( part1[i] == '=' ) 
			{ 
						 temp[j] = '\0'; 
						 break; //
			}                                    
			temp[j++] = part1[i]; 
		} 
		// + "="
		for ( i = 0 + strlen(temp), j = 0; i < (int)strlen(part1); i++ ) //特别
		{ 
			ways2[j++] = part1[i]; 
		} 
		ways2[j] = '\0';
		j = 0;
		memset(temp,0,32);
		////------------------------
		for ( i = 0; i < (int)strlen(part2); i++ ) 
		{ 
			if ( part2[i] == '=' ) 
			{ 
						 temp[j] = '\0'; 
						 break; //
			}                                    
			temp[j++] = part2[i]; 
		} 
		// + "="
		for ( i = 1 + strlen(temp), j = 0; i < (int)strlen(part2); i++ ) 
		{ 
			session_id_t[j++] = part2[i]; 
		} 
		session_id_t[j] = '\0';
		j = 0;
		memset(temp,0,32);
		////------------------------
		for ( i = 0; i < (int)strlen(part3); i++ ) 
		{ 
			if ( part3[i] == '=' ) 
			{ 
						 temp[j] = '\0'; 
						 break; //
			}                                    
			temp[j++] = part3[i]; 
		} 
		// + "="
		for ( i = 1 + strlen(temp), j = 0; i < (int)strlen(part3); i++ ) 
		{ 
			page[j++] = part3[i]; 
		} 
		page[j] = '\0';
		j = 0;
		memset(temp,0,32);
		////------------------------
		for ( i = 0; i < (int)strlen(part4); i++ ) 
		{ 
			if ( part4[i] == '=' ) 
			{ 
						 temp[j] = '\0'; 
						 break; //
			}                                    
			temp[j++] = part4[i]; 
		} 
		// + "="
		for ( i = 1 + strlen(temp), j = 0; i < (int)strlen(part4); i++ ) 
		{ 
			request[j++] = part4[i]; 
		} 
		request[j] = '\0';
		j = 0;
		memset(temp,0,32);
		
		i_page = atoi(page);
		
		//
		stpcpy(session_id,session_id_t);
		
		start_session(session_id);
		printf("%s\r\n\r\n","Content-Type:text/html");  //默认第一行输出
		
		//操作
		
		
		if( (fp_html=fopen("../tp/tp_xg_record_html","r")) == NULL)
		{
			exit(1);
		}
		while(fgets(buf,512,fp_html))
		{
			if(strncmp(buf,"$FLAG1$",7)==0)
			{
				printf("<li><a href='welcome_session.cgi?session_id=%s'>主页</a></li>",session_id);
				printf("<li><a href='current_msg_2.cgi?session_id=%s'>实时信息</a></li>",session_id);
				printf("<li><a href='xg_record_2.cgi?session_id=%s'>巡更数据</a></li>",session_id);
				printf("<li><a href='device_registered_2.cgi?session_id=%s'>设备列表</a></li>",session_id);
				printf("<li><a href='device_requested_show_2.cgi?session_id=%s'>授权列表</a></li>",session_id);
				printf("<li><a href='device_requested_2.cgi?session_id=%s'>设备授权</a></li>",session_id);
				printf("<li><a href='device_unrequested_show_2.cgi?session_id=%s'>注销列表</a></li>",session_id);
				printf("<li><a href='device_unrequested_2.cgi?session_id=%s'>设备注销</a></li>",session_id);
				printf("<li><a href='data_manage_2.cgi?session_id=%s'>数据库管理</a></li>",session_id);
				printf("<li><a href='destroy_session.cgi?session_id=%s'>登出</a></li>",session_id);
			}
			else if(strncmp(buf,"$FLAG2$",7)==0)
			{
				printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
				printf("<input type='submit' name='submit' value='确定' />\n");
				printf("</form>");
				
				//数据库查询
				
				//------------------------------------------------------------------------------------
		
				sqlite3 *db = NULL; 
				char *zErrMsg = 0; 
				int rc; 
				char *sql;
				
				rc = sqlite3_open("/myapps/dbs/zigbee.db", &db); //打开数据库

				//创建表格，若存在，则打开
				sql = " create table xg_record(id integer primary key,thisPoint text,who text,createdTime timeStamp not null default(datetime('now','localtime'))); " ; 
				sqlite3_exec( db , sql , 0 , 0 , &zErrMsg ); 
				
				//选择sql
				if(strcmp(ways2,"s1")==0)	
				{
					sql = "SELECT * FROM xg_record WHERE DATE('now', '-3 day', 'localtime') < DATE(createdTime) < DATE('now', '-0 day', 'localtime');";
					i_section = 1;
				}
				else if(strcmp(ways2,"s2")==0)
				{
					sql = "SELECT * FROM xg_record WHERE DATE('now', '-5 day', 'localtime') < DATE(createdTime) < DATE('now', '-0 day', 'localtime');";
					i_section = 2;
				}
				else if(strcmp(ways2,"s3")==0)
				{
					sql = "SELECT * FROM xg_record WHERE DATE('now', '-7 day', 'localtime') < DATE(createdTime) < DATE('now', '-0 day', 'localtime');";
					i_section = 3;
				}
				else if(strcmp(ways2,"s4")==0)
				{
					sql = " SELECT * FROM xg_record WHERE who = '王小明'; ";
					i_section = 4;
				}
				else if(strcmp(ways2,"s5")==0)
				{
					sql = " SELECT * FROM xg_record WHERE who = '王小东'; ";
					i_section = 5;
				}
				else if(strcmp(ways2,"s6")==0)
				{
					sql = " SELECT * FROM xg_record WHERE who = '王小西'; ";
					i_section = 6;
				}
				else if(strcmp(ways2,"s7")==0)
				{
					sql = " SELECT * FROM xg_record; ";
					i_section = 7;
				}
				else sql = "";
				
				//test
				/* printf("sql:%s",sql); */
				
				//行，列，结果
				int row=0,column=0;  
				char **result;    

				//nextpage编程标志
				int pages = 0;           //
				int records = 0;         //最后一页有多少条记录
				int all_page = 0;        //共all_page页
				
				sqlite3_get_table(db,sql,&result,&row,&column,&zErrMsg);//获得表格数据

				//nextpage编程标志
				pages = row/15;
				records = row%15;
				
				//共all_page页
				if((pages==0)&&(records==0))
					all_page = 0;
				if((pages==0)&&(records>0))
					all_page = 1;
				if((pages>0)&&(records==0))
					all_page = pages;
				if((pages>0)&&(records>0))
					all_page = pages+1;
				
				//test
				/* printf("pages=%d",pages);
				printf("records=%d",records);
				printf("all_page=%d",all_page); */
				
				//导出txt
				fp_text=fopen("../txt/xg_save.txt","w");
				fprintf(fp_text,"ID    本节点    巡更人    时间\n");
				for (i = 1; i < (row+1); i++)
				{
					fprintf(fp_text,"%s    %s    %s    %s    %s\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
				}	
				fclose(fp_text);
		
				//nextpage编程标志
				if ((pages==0)||((pages==1)&&(records==0)))  //查询到的记录只有一页
				{
					//分类
					switch(i_section)
					{
						case 1:
							printf("<p>三日内的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1; i < (row+1); i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}		
							printf("</table>\n");
							break;
						case 2:
							printf("<p>五日内的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1; i < (row+1); i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}		
							printf("</table>\n");
							break;
						case 3:
							printf("<p>七日内的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1; i < (row+1); i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;
						case 4:
							printf("<p>王小明的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1; i < (row+1); i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;	
						case 5:
							printf("<p>王小东的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1; i < (row+1); i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;	
						case 6:
							printf("<p>王小西的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1; i < (row+1); i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;	
						case 7:
							printf("<p>全部节点的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1; i < (row+1); i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;		
						
					}
				}

				if ((((pages==1)&&(records>0))||(pages>1))&&(i_page<(pages+1)))  //查询到的记录不止一页，且当前不是最后一页
				{
				
					//分类
					switch(i_section)
					{
						case 1:
							printf("<p>三日内的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < i_page*15+1; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}		
							printf("</table>\n");
							break;
						case 2:
							printf("<p>五日内的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < i_page*15+1; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}		
							printf("</table>\n");
							break;
						case 3:
							printf("<p>七日内的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < i_page*15+1; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;
						case 4:
							printf("<p>王小明的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < i_page*15+1; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;	
						case 5:
							printf("<p>王小东的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < i_page*15+1; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;	
						case 6:
							printf("<p>王小西的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < i_page*15+1; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;	
						case 7:
							printf("<p>全部节点的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < i_page*15+1; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;		
						
					}
						
				}

				if ((((pages==1)&&(records>0))||(pages>1))&&(i_page==(pages+1)))  //查询到的记录不止一页，且当前是最后一页
				{
					//分类
					switch(i_section)
					{
						case 1:
							printf("<p>三日内的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < 1+(i_page-1)*15+records; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}		
							printf("</table>\n");
							break;
						case 2:
							printf("<p>五日内的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < 1+(i_page-1)*15+records; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}		
							printf("</table>\n");
							break;
						case 3:
							printf("<p>七日内的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < 1+(i_page-1)*15+records; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;
						case 4:
							printf("<p>王小明的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < 1+(i_page-1)*15+records; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;	
						case 5:
							printf("<p>王小东的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < 1+(i_page-1)*15+records; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;	
						case 6:
							printf("<p>王小西的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < 1+(i_page-1)*15+records; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;	
						case 7:
							printf("<p>全部节点的巡更记录：</p>\n");
							printf("<table id='Table1' border='1' width='500'>\n<tr>\n<td>ID</td>\n<td>本节点</td>\n<td>巡更人</td>\n<td>时间</td>\n</tr>\n");
							for (i = 1+(i_page-1)*15; i < 1+(i_page-1)*15+records; i++)
							{
								printf("<tr>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n<td>%s</td>\n</tr>\n",result[4*i],result[4*i+1],result[4*i+2],result[4*i+3]);
							}				
							printf("</table>\n");
							break;		
						
					}			
				}

				//导出excel
				printf("<input id='Submit1' type='submit' value='导入到EXCEL' onclick='PrintTableToExcel(Table1)' />");	
				
				//------------------------------------------------------------------------------------
				
				if (i_page>1)
				{
					printf("<form name='form2' action='/cgi-bin/xg_record_2.cgi' method='post'>\n");
					printf("<input type='hidden' name='ways2' value='%s'/>",ways2);
					printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
					printf("<input type='hidden' name='page' value='%d'/>",(i_page-1));
					printf("<input type='submit' name='request' value='上一页' />\n");
					printf("</form>");	
				}				
				if ((i_page>0)&&(i_page<all_page))
				{
					printf("<form name='form2' action='/cgi-bin/xg_record_2.cgi' method='post'>\n");
					printf("<input type='hidden' name='ways2' value='%s'/>",ways2);
					printf("<input type='hidden' name='session_id' value='%s'/>",session_id);
					printf("<input type='hidden' name='page' value='%d'/>",(i_page+1));
					printf("<input type='submit' name='request' value='下一页' />\n");
					printf("</form>");
				}
				
				printf("&nbsp;&nbsp;");
				printf("共%d页，当前为第%d页。",all_page,i_page);
				
				//下载txt
				printf("<a href='../txt/xg_save.txt' download='xg_save'>\n");
				printf("点击下载表单</a>\n");
					
			}
			else
				printf("%s",buf);
			
		}	
	}
	else
		;
 
	return 0; 
         
} 




char* getcgidata(FILE* fp, char* requestmethod) 
{ 
	char* input; 
	int len; 
	int size = 1024; 
	int i = 0; 
	
	if (!strcmp(requestmethod, "GET")) 
	{ 
		input = getenv("QUERY_STRING"); 
		return input; 
	} 
	else if (!strcmp(requestmethod, "POST")) 
	{ 
		len = atoi(getenv("CONTENT_LENGTH")); 
		input = (char*)malloc(sizeof(char)*(size + 1)); 
	 
		if (len == 0) 
		{ 
			input[0] = '\0'; 
			return input; 
		} 
	 
		while(1) 
		{ 
			input[i] = (char)fgetc(fp); 
			if (i == size) 
			{ 
				input[i+1] = '\0'; 
				return input; 
			} 
			
			--len; 
			if (feof(fp) || (!(len))) 
			{ 
				i++; 
				input[i] = '\0'; 
				return input; 
			} 
			i++; 
			
		} 
	} 
	return NULL; 
}
